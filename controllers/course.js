const Course = require('../models/Course');
const bcrypt = require("bcrypt");
const auth = require('../auth');


 
/*module.exports.addNewCourse = (params) => {
	let newCourse = new Course({
		name: params.name,
		description: params.description,
		price: params.price
		
	});

return newCourse.save().then((course, err) => {
		return (err) ? false : true
		
	});
}
*/
/*module.exports.get=() => {
    return Course.find({isActive:true}).then(course => {
        return course
    })

}*/

//pwede din instead of params params.courseid
// module.exports.getCourse=(params) => {
	

// return Course.findById(params).then(course => {
		
// 		return course
// 	})
	

// }

/*module.exports.getCourse=(params) => {
	

return Course.findById(params).then(course => {
		
		return course
	})
	

}*/


/*module.exports.updateCourse=(params, req) => {
	
return Course.findById(params).then(course => {
		course.name= req.body.name
		course.description = req.body.description
		course.price= req.body.price
		return course.save().then((course,err)=>{
			return(err)? false: true
		})
	})
	

}*/
/*module.exports.dCourse = (params) => {
    //find a user
    return Course.findById(params).then(course => {
    // deactivate course
        course.isActive = false;
    //save to database
    return course.save().then((course,err)=>{
        return(err)? false: true    
       })
    })
}*/

//SOLUTION BY SIR ARVIN

//adding a course
module.exports.add = (params) => {
	let course = new Course({
		name: params.name,
		description: params.description,
		price: params.price
	})

	return course.save().then((course, err) => {
		return (err) ? false : true
	})
}

//Get All
module.exports.getAll = () => {
	return Course.find({ isActive: true }).then(courses => courses)
}

//Get By ID
module.exports.get = (params) => {
	return Course.findById(params.courseId).then(course => course)
}

module.exports.update = (params) => {
	const updates = {
		name: params.name,
		description: params.description,
		price: params.price
	}

	return Course.findByIdAndUpdate(params.courseId, updates).then((doc, err) => {
		return (err) ? false : true
	})
}

module.exports.archive = (params) => {
	const updates = { isActive: false }

	return Course.findByIdAndUpdate(params.courseId, updates).then((doc, err) => {
		return (err) ? false : true
	})
}


