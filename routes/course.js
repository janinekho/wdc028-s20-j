const express = require ('express');
const router = express.Router();
const CourseController = require('../controllers/course');
const auth = require('../auth');


//Adding Courses
/*router.post('/', (req, res) => {
	CourseController.addNewCourse(req.body).then(resultFromAddNewCourse => res.send(resultFromAddNewCourse));
});*/



/*//Retrieving all courses
router.get('/all',(req, res)=>{
    CourseController.get({isActive:true}).then(resultFromDetails => res.send(resultFromDetails))
})*/

//Retrieving individual courses

/*router.get('/:courseid', (req, res)=>{

	const course = auth.decode(req.headers.authorization)

    CourseController.get({_id: courseid}).then(resultFromCourseId => res.send(resultFromCourseId))
})*/

// //Retreieving a course
// router.get('/:courseid', (req, res)=>{

// const courseid = req.params.courseid
// CourseController.getCourse(courseid).then(resultFromCourseId => res.send(resultFromCourseId))
// })


//Updating the Course

/*router.put('/:courseid', auth.verify, (req, res)=>{
 const courseid = req.params.courseid
CourseController.updateCourse(courseid,req).then(resultFromCourseId => res.send(resultFromCourseId))

})*/


//Soft delete
/*router.delete('/disable/:courseid',(req,res) =>{
const courseid = req.params.courseid
CourseController.dCourse(courseid).then(resultFromCourseId => res.send(resultFromCourseId))
})*/


	

/*router.delete('/:courseid', (req, res) => {
    const courseid = req.params.courseid
    CourseController.delete(courseid).then(result => res.send(result))
})*/



//SIR ARVIN SOLUTION



//Adding a course
router.post('/', (req, res) => {
    CourseController.add(req.body).then(result => res.send(result))
})

//Get All
router.get('/', (req, res) => {
    CourseController.getAll().then(courses => res.send(courses))
})

//geting a course by an id
//courseId = string
router.get('/:courseId', (req, res) => {
	const courseId = req.params.courseId
	//it has not key value kasi declared na sa variable itself. dito object
    CourseController.get({ courseId }).then(course => res.send(course)) 
})

router.put('/', auth.verify, (req, res) => {
    CourseController.update(req.body).then(result => res.send(result))
})

router.delete('/:courseId', auth.verify, (req, res) => {
	const courseId = req.params.courseId
    CourseController.archive({ courseId }).then(result => res.send(result))
})

module.exports = router
